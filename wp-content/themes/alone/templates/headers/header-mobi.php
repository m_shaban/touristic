<?php
$_FW = defined( 'FW' );
$header_options = alone_builder_options_header();
extract($header_options);

$alone_logo_settings = defined( 'FW' ) ? fw_get_db_customizer_option( 'logo_settings' ) : array();
$alone_logo_retina = defined( 'FW' ) ? fw_akg('logo/image/retina_logo', $alone_logo_settings) : '';// $alone_logo_settings['logo']['image']['retina_logo'];
$header_class_arr = array( basename( __FILE__, '.php' ), $alone_logo_retina, 'fw-menu-position-right', $alone_absolute_header );
?>
<!--<header class="bt-header-mobi <?php echo esc_attr( implode( ' ',  $header_class_arr ) ); ?>" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	 Header main menu 
	<div class="bt-header-mobi-main">
		<div class="container">
			<div class="bt-container-logo bt-vertical-align-middle">
				<?php alone_logo(); ?>
			</div>
			<div class="bt-container-menu bt-vertical-align-middle">
				<div class="bt-nav-wrap" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement" role="navigation">
					<?php
					if( ! $_FW ) { ?>
						<div class="default-mobi-menu-wrap" data-default-menu-mobi-handle>
							<button class="button-toggle-ui"><?php _e('Menu', 'alone'); ?></button>
							<?php fw_theme_nav_menu( 'mobi_menu' ); ?>
						</div>
					<?php } else {
						fw_theme_nav_menu( 'mobi_menu' );
					}
					?>
				</div>
			</div>
		</div>
	</div>
</header>-->
<div class="container" >
            <div class="header nav-down" id="hd">
                <div class="row">
                    <div class="topheader">
                        <div class="col-md-6">
                            <div class="logo">
                                <?php alone_logo(); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="reg">
                                <li>
                                    <a href="#">Login</a>
                                    <a href="#">Register</a>
                                </li>
                            </ul>
                            <div id="mySidenav" class="sidenav">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                <a href="index.html" class="active"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                                <a href="about-us.html"><i class="fa fa-info"></i> About us </a>
                                <a href="kuwait-towers.html"><i class="fa fa-ship"></i> Tourism Sites </a>
                                <a href="#"><i class="fa fa-address-book-o"></i> Booking </a>
                                    <a href="calender-event.html"><i class="fa fa-calendar"></i> Calender </a>
                                     
                                            <a href="#!"><i class="fa fa-calendar"></i> Book Now</a>
                                 
                               
                                <a href="contactus.html"><i class="fa fa-phone"></i>Contact Us</a>
                                <div class="register">
                                    <a href="#"> <i class="fa fa-sign-in"></i> Login </a>
                                    <a href="#"> <i class="fa fa-sign-out"></i> Register </a>
                                </div>
                            </div>

                            <span class="showhide" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>

                        </div>
                    </div>
                </div>
                <div class="row">
                    
                        <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
                            


                            <?php fw_theme_nav_menu( 'mobi_menu' ); ?>
                            

                              
                    
                </div>
            </div>
        </div>
