<?php
$header_options = alone_builder_options_header();
extract($header_options);

$header_class_arr = array( basename( __FILE__, '.php' ), /* $alone_header_logo_align, */ $alone_header_full_content, ''/*$alone_header_menu_position*/ );
$header_container_class_arr = array( $alone_absolute_header, $alone_sticky_header );
$header_class_container = !empty($alone_header_full_content) ? 'container-fluid' : 'container';

// echo '<pre>'; print_r($alone_header_settings); echo '</pre>';
?>
<div class="container" >
            <div class="header nav-down" id="hd">
                <div class="row">
                    <div class="topheader">
                        <div class="col-md-6">
                            <div class="logo">
                                <?php alone_logo(); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="reg">
                                <li>
                                    <a href="#">Login</a>
                                    <a href="#">Register</a>
                                </li>
                            </ul>
                            <div id="mySidenav" class="sidenav">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                <a href="index.html" class="active"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                                <a href="about-us.html"><i class="fa fa-info"></i> About us </a>
                                <a href="kuwait-towers.html"><i class="fa fa-ship"></i> Tourism Sites </a>
                                <a href="#"><i class="fa fa-address-book-o"></i> Booking </a>
                                    <a href="calender-event.html"><i class="fa fa-calendar"></i> Calender </a>
                                     
                                            <a href="#!"><i class="fa fa-calendar"></i> Book Now</a>
                                 
                               
                                <a href="contactus.html"><i class="fa fa-phone"></i>Contact Us</a>
                                <div class="register">
                                    <a href="#"> <i class="fa fa-sign-in"></i> Login </a>
                                    <a href="#"> <i class="fa fa-sign-out"></i> Register </a>
                                </div>
                            </div>

                            <span class="showhide" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <nav>
                        <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
                        <?php alone_header_mobile_menu(); ?>
                        <ul class="nav-list">
                            <li>
                                <a href="index.html" class="active">Home</a>
                            </li>
                            <li>
                                <a href="about-us.html">About us </a>
                            </li>
                            <li>
                                <a href="kuwait-towers.html">Tourism Sites </a>

                            </li>
                            <li>
                                <a href="booking.html">Booking </a>
                            </li>
                            <li>
                                <a href="#!">Events</a>
                                <ul class="nav-dropdown">
                                    <li>
                                        <a href="calender-event.html">Calender </a>
                                    </li>
                                    <li>
                                        <a href="#!">Book Now</a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a href="contactus.html">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>