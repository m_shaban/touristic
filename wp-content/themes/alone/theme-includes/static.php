<?php if (!defined('ABSPATH')) {
	die('Direct access forbidden.');
}
/**
 * Include static files: javascript and css
 */

$alone_template_directory = get_template_directory_uri();
if (defined('FW')) {
	$alone_version = fw()->theme->manifest->get_version();
} else {
	$alone_version = '1.0';
}

/**
 * Enqueue scripts and styles for the front end.
 */

// Load jquery
// wp_enqueue_script( 'jquery' );

// jquery ui resizable
wp_enqueue_script( 'jquery-ui-resizable' );

if (is_singular() && comments_open() && get_option('thread_comments')) {
	wp_enqueue_script('comment-reply');
}



add_editor_style();

// Load local font
wp_enqueue_style(
	'font-awesome',
	'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
	array(),
	$alone_version
);

// Load theme stylesheet.

wp_enqueue_style(
	'alone-style',
	$alone_template_directory . '/assets/css/styles.css',
	array(),
	$alone_version
);
wp_enqueue_style(
	'alone-home-style',
        $alone_template_directory . '/assets/css/home.css',
	array(),
	$alone_version
);
wp_enqueue_style(
	'alone-theme-style',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
	array(),
	$alone_version
);
// Load theme script
wp_enqueue_script(
	'alone-jquery-script',
	'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
	array( 'jquery' ),
	$alone_version,
	true
);
wp_enqueue_script(
	'alone-bootstrap-script',
	'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
	array( 'jquery' ),
	$alone_version,
	true
);
wp_localize_script('alone-theme-script', 'BtPhpVars', array(
	'ajax_url' => admin_url('admin-ajax.php'),
	'template_directory' => $alone_template_directory,
	'previous' => esc_html__('Previous', 'alone'),
	'next' => esc_html__('Next', 'alone'),
	'smartphone_animations' => function_exists('fw_get_db_settings_option') ? fw_get_db_settings_option('enable_smartphone_animations', 'no') : 'no',
	'fail_form_error' => esc_html__('Sorry you are an error in ajax, please contact the administrator of the website', 'alone'),
));
