    <div class="footer">
        <div class="container">
           
                <div class="col-md-6">
                    <p style="color:#fff;">Copyright 2015  © TEC .  All rights reserved. </p>
                    <ul class="infoo"> 
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">FAQ </a></li>
                    </ul>
                </div>
            
       
                <div class="col-md-6">

                    <ul class="socialmedia">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ins" href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a class="tw" href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a class="yo" href="#">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ln" href="#">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
     
    </div>

    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.ib1 ').mouseover(function () {
                $(".e1").css("background-color", "#ab204e");
                $(".e1").css("color", "#fff");
                $(".e1 span").css("color", "#fff");

            });
            $('.ib1').mouseout(function () {

                $(".e1").css("background-color", "rgba(255,255,255,0.7)");
                $(".e1").css("color", "#666666");
                $(".e1 span").css("color", "#000");
            });

            $('.ib2').mouseover(function () {
                $(".e2").css("background-color", "#ab204e");
                $(".e2").css("color", "#fff");
                $(".e2 span").css("color", "#fff");

            });
            $('.ib2').mouseout(function () {

                $(".e2").css("background-color", "rgba(255,255,255,0.7)");
                $(".e2").css("color", "#666666");
                $(".e2 span").css("color", "#000");
            });

            $('.ib3').mouseover(function () {
                $(".e3").css("background-color", "#ab204e");
                $(".e3").css("color", "#fff");
                $(".e3 span").css("color", "#fff");
            });
            $('.ib3').mouseout(function () {

                $(".e3").css("background-color", "rgba(255,255,255,0.7)");
                $(".e3").css("color", "#666666");
                $(".e3 span").css("color", "#000");
            });


            $('.ib4 ').mouseover(function () {
                $(".e4").css("background-color", "#ab204e");
                $(".e4").css("color", "#fff");
                $(".e4 span").css("color", "#fff");

            });
            $('.ib4').mouseout(function () {

                $(".e4").css("background-color", "rgba(255,255,255,0.7)");
                $(".e4").css("color", "#666666");
                $(".e4 span").css("color", "#000");
            });

            $('.ib5').mouseover(function () {
                $(".e5").css("background-color", "#ab204e");
                $(".e5").css("color", "#fff");
                $(".e5 span").css("color", "#fff");

            });
            $('.ib5').mouseout(function () {

                $(".e5").css("background-color", "rgba(255,255,255,0.7)");
                $(".e5").css("color", "#666666");
                $(".e5 span").css("color", "#000");
            });

            $('.ib6').mouseover(function () {
                $(".e6").css("background-color", "#ab204e");
                $(".e6").css("color", "#fff");
                $(".e6 span").css("color", "#fff");
            });
            $('.ib6').mouseout(function () {

                $(".e6").css("background-color", "rgba(255,255,255,0.7)");
                $(".e6").css("color", "#666666");
                $(".e6 span").css("color", "#000");
            });

            $('.ib7').mouseover(function () {
                $(".e7").css("background-color", "#ab204e");
                $(".e7").css("color", "#fff");
                $(".e7 span").css("color", "#fff");
            });
            $('.ib7').mouseout(function () {

                $(".e7").css("background-color", "rgba(255,255,255,0.7)");
                $(".e7").css("color", "#666666");
                $(".e7 span").css("color", "#000");
            });
            $('.ib8').mouseover(function () {
                $(".e8").css("background-color", "#ab204e");
                $(".e8").css("color", "#fff");
                $(".e8 span").css("color", "#fff");
            });
            $('.ib8').mouseout(function () {

                $(".e8").css("background-color", "rgba(255,255,255,0.7)");
                $(".e8").css("color", "#666666");
                $(".e8 span").css("color", "#000");
            });
            $('.ib9').mouseover(function () {
                $(".e9").css("background-color", "#ab204e");
                $(".e9").css("color", "#fff");
                $(".e9 span").css("color", "#fff");
            });
            $('.ib9').mouseout(function () {

                $(".e9").css("background-color", "rgba(255,255,255,0.7)");
                $(".e9").css("color", "#666666");
                $(".e9 span").css("color", "#000");
            });
        });


    </script>

   
   <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : 'your-app-id',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.12'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
    </script>

 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <script>
        $(function () {

            $('.toggleButton').click(function () {

                var target = $('#' + $(this).attr('data-target'));
                $('.toggleDiv').not(target).hide();
                target.show();
            });
        });
    </script>

    <script>
        (function ($) { // Begin jQuery
            $(function () { // DOM ready
                // If a link has a dropdown, add sub menu toggle.
                $('nav ul li a:not(:only-child)').click(function (e) {
                    $(this).siblings('.nav-dropdown').toggle();
                    // Close one dropdown when selecting another
                    $('.nav-dropdown').not($(this).siblings()).hide();
                    e.stopPropagation();
                });
                // Clicking away from dropdown will remove the dropdown class
                $('html').click(function () {
                    $('.nav-dropdown').hide();
                });
                // Toggle open and close nav styles on click
                $('#nav-toggle').click(function () {
                    $('nav ul').slideToggle();
                });
                // Hamburger to X toggle
                $('#nav-toggle').on('click', function () {
                    this.classList.toggle('active');
                });
            }); // end DOM ready
        })(jQuery); // end jQuery
    </script>
    <script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
    </script>

    <script>
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = $('#hd').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('#hd').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('#hd').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }
    </script>

</body>
</html>
