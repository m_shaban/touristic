<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tour');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TFQU,S WN9oxiumThI9e4~FH`>-?vc~t/>4:uK.U&xrR=NAziidX;`e}r{y- AOj');
define('SECURE_AUTH_KEY',  '7`:^/M_=;$#RF?)^&Rhu?SCzFH$*|#a64|v*3az,F1!Avj.RG7n09eyrtPrm 4xu');
define('LOGGED_IN_KEY',    '~~@,JW+i)8GXH~n+qxx><V)<w;E}a+)-t^yuk/,9yPMZ.W={%95F}}Rb+%wYE^Ll');
define('NONCE_KEY',        'a@EOA9?17e&N7c9<=~%M=&3.0`2Eb(f{opbL*:`i:}J|2xbZJ~M0D4<L+U1IswCi');
define('AUTH_SALT',        '$R}j__*[xTna9i}<.@1H[q4.K!_7djL/.dfK~HO_uB:b/HlsTm#~/GFX./$U(npD');
define('SECURE_AUTH_SALT', 'X&=E[0P{-Xp5FE!=74@(qD[TQ!f45GG09kee5!LCY8*hVj=4`Ohj?dPV_nmgZ$|J');
define('LOGGED_IN_SALT',   'r{t&tTeCKLdpKOh_ixP+K#KKB+X<U%^/6`f}E0Yd:SfOZ2mP|BwsdXDhQX4%VikC');
define('NONCE_SALT',       'GT|.#vO{F!eD??<!#1C/2.Kej u3@<_:aLFnr9[ksk.hQ2~f!Dn|tc|qo$4g]*U;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
